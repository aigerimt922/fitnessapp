CREATE TABLE IF NOT EXISTS users
(
    id   serial primary key,
    bio      varchar(255),
    email    varchar(255) unique,
    password varchar(255),
    username varchar(255) unique
);

CREATE TABLE IF NOT EXISTS posts
(
    id     serial  primary key,
    brief         varchar(255),
    cover         varchar(255),
    creation_date timestamp,
    description   oid,
    title         varchar(255),
    weight        integer,
    author_id     int
        constraint fk_post_author
            references users
);

CREATE TABLE IF NOT EXISTS roles
(
    id serial primary key,
    description varchar(255),
    name        varchar(255) unique,
    title       varchar(255),
    weight      integer unique
);

CREATE TABLE IF NOT EXISTS tags
(
    id  serial primary key,
    name varchar(255) unique
);

CREATE TABLE IF NOT EXISTS posts_tags
(
    posts_id int not null
        constraint fk_post_tag
            references posts,
    tags_id  int not null
        constraint fk_tag_post
            references tags
);

CREATE TABLE IF NOT EXISTS users_roles
(
    users_id int not null
        constraint fk_user_role
            references users,
    roles_id int not null
        constraint fk_role_user
            references roles,
    primary key (users_id, roles_id)
);

CREATE TABLE IF NOT EXISTS subscriptions
(
    id  serial primary key,
    author varchar(255),
    reader varchar(255),
    role_id     int
        constraint fk_role_subscription
            references roles
);