do $$
    declare
        var_user_id integer;
    declare
        var_role_id integer;
    begin

        INSERT INTO users (email,username, password)
        VALUES   ('aigerimt922@gmail.com',
                      'ATumbayeva',
                      '$2a$10$RM2R9u6uvrYDmD7X6B/oOOvd6O2vLDfSbZhfvOPjbQQ21CXHmHgU6');

        INSERT INTO roles (description, name, title, weight)
        VALUES ('', 'ROLE_ADMIN','Admin',-2);

        -- select user id
        select id
        into var_user_id
        from users
        where email='aigerimt922@gmail.com';

        -- select user id
        select id
        into var_role_id
        from roles
        where name='ROLE_ADMIN';

        -- insert associative
        INSERT INTO users_roles
        (users_id, roles_id) VALUES (var_user_id, var_role_id);
    end $$;