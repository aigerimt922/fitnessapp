package com.epam.capstone.services;

import com.epam.capstone.dto.SubscriptionDTO;
import com.epam.capstone.models.Subscription;

import java.util.List;

public interface SubscriptionService {

    List<Subscription> findAllSubs();

    void deleteSub(int id);

    void saveSub(SubscriptionDTO subscriptionDTO);

    List<Subscription> findByAuthorAndReader(String author, String reader);
}
