package com.epam.capstone.services;

import com.epam.capstone.dto.RoleDTO;
import com.epam.capstone.models.Role;

import java.util.List;

public interface RoleService {

    Role getDefaultRole();
    Role getModeratorRole();
    List<Role> findAllRoles();

    Role findRole(int id);

    void deleteRole(int id);

    void saveRole(RoleDTO role);
}
