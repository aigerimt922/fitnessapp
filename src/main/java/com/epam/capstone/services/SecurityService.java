package com.epam.capstone.services;

import com.epam.capstone.models.Post;

public interface SecurityService {
    boolean isAuthenticated();

    void autoLogin(String username, String password);

    boolean isAccessible(Post post);

    void makeModerator();
}
