package com.epam.capstone.services;

import com.epam.capstone.dto.PostDTO;
import com.epam.capstone.dto.PostPreviewDTO;
import com.epam.capstone.models.Post;
import com.epam.capstone.models.Tag;
import com.epam.capstone.models.User;

import java.util.List;

public interface PostService {
    List<PostPreviewDTO> findAllPostsByAuthor(User user);

    List<PostPreviewDTO> findAllWithTagAndAuthor(Tag tag, User user);

    Post findPost(Integer id);

    Post savePost(PostDTO post);

    void deletePost(Integer id);
}
