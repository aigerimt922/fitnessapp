package com.epam.capstone.services.impls;

import com.epam.capstone.dto.RoleDTO;
import com.epam.capstone.mappers.RoleMapper;
import com.epam.capstone.models.Role;
import com.epam.capstone.models.User;
import com.epam.capstone.repositories.RoleRepository;
import com.epam.capstone.services.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Locale;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;

    @Value("${weight.exist}")
    private String weightExist;

    @Value("${role.has.reference}")
    private String hasReferences;

    @Override
    public Role getDefaultRole() {
        Role role = roleRepository.findByName("ROLE_USER");
        if (role == null) {
            role = Role.builder()
                    .name("ROLE_USER")
                    .weight(1)
                    .description("Posts with this level can be opened if you registered")
                    .title("Registered User")
                    .build();
            roleRepository.save(role);
        }
        return role;
    }

    @Override
    public Role getModeratorRole() {
        Role role = roleRepository.findByName("ROLE_MODERATOR");
        if (role == null) {
            role = Role.builder()
                    .name("ROLE_MODERATOR")
                    .weight(-1)
                    .build();
            roleRepository.save(role);
        }
        return role;
    }

    @Override
    public List<Role> findAllRoles() {
        return roleRepository.findAllExcept();
    }

    @Override
    public Role findRole(int id) {
        return roleRepository.findById(id).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public void deleteRole(int id) {
        Role role = roleRepository.findById(id).orElse(new Role());
        if (role.getUsers() != null && !role.getUsers().isEmpty()) {
            throw new IllegalArgumentException(hasReferences);
        }
        roleRepository.deleteById(id);
    }

    @Override
    public void saveRole(RoleDTO role) {
        if (roleRepository.findByWeight(role.getWeight()) != null) {
            throw new IllegalArgumentException(weightExist);
        }

        Role newRole = RoleMapper.MAPPER.roleDtoToRole(role);
        newRole.setName("ROLE_" + newRole.getName().toUpperCase(Locale.ROOT));

        if (role.getId() != null) {
            Set<User> users = roleRepository.findById(role.getId()).get().getUsers();
            newRole.setUsers(users);
        }

        roleRepository.save(newRole);
    }
}
