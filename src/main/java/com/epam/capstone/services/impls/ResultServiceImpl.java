package com.epam.capstone.services.impls;

import com.epam.capstone.dto.UserRegisterDTO;
import com.epam.capstone.models.Post;
import com.epam.capstone.services.PostService;
import com.epam.capstone.services.ResultService;
import com.epam.capstone.services.SecurityService;
import com.epam.capstone.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

@Service
@RequiredArgsConstructor
public class ResultServiceImpl implements ResultService {

    private final SecurityService securityService;
    private final PostService postService;
    private final UserService userService;

    @Value("${page.not.found}")
    private String notFound;
    @Value("${service.problem}")
    private String server;
    @Value("${access.post.denied}")
    private String accessDenied;
    @Value("${post.not.yours}")
    private String notYours;

    @Value("${username.exist}")
    private String usernameExist;

    @Value("${email.exist}")
    private String emailExist;

    @Value("${email.support}")
    private String emailSupport;

    private static final String ERROR_STATUS = "errorStatus";

    private static final String ERROR_TEXT = "errorText";

    private static final String EMAIL_TEXT = "email";

    private static final String USER_FORM = "userForm";

    @Override
    public void populateResultByStatus(Object status, Model model) {
        if (status != null) {
            int statusCode = Integer.parseInt(status.toString());
            model.addAttribute(ERROR_STATUS, status);
            if (statusCode == HttpStatus.NOT_FOUND.value()) {
                model.addAttribute(ERROR_TEXT, notFound);
            } else if (statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
                model.addAttribute(ERROR_TEXT, server);
                model.addAttribute(EMAIL_TEXT, emailSupport);
            } else {
                model.addAttribute(ERROR_TEXT, "Something went wrong");
            }
        }
    }

    @Override
    public String getPageAndAccessPostResult(Post post, Model model) {
        if (securityService.isAccessible(post)) {
            model.addAttribute("post", post);
            return "post";
        } else {
            model.addAttribute(ERROR_STATUS, "access denied");
            model.addAttribute(ERROR_TEXT, accessDenied);
            model.addAttribute(EMAIL_TEXT, post.getAuthor().getEmail());
            return "error";
        }
    }

    @Override
    public boolean isPostForbidden(String postId, Model model) {
        if (postId != null && !postId.isEmpty()) {
            Post post = postService.findPost(Integer.valueOf(postId));
            if (!userService.checkIfMine(post)) {
                model.addAttribute(ERROR_STATUS, "not yours");
                model.addAttribute(ERROR_TEXT, notYours);
                return true;
            }
            model.addAttribute("post", post);
        } else {
            model.addAttribute("post", new Post());
        }
        return false;
    }

    @Override
    public boolean isRegistrationFailed(UserRegisterDTO userForm, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return true;
        }
        if (userService.findByUsername(userForm.getUsername()) != null) {
            bindingResult.addError(new FieldError(USER_FORM, "username", usernameExist));
            return true;
        }
        if (userService.findByEmail(userForm.getEmail()) != null) {
            bindingResult.addError(new FieldError(USER_FORM, EMAIL_TEXT, emailExist));
            return true;
        }

        return false;
    }
}
