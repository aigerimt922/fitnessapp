package com.epam.capstone.services.impls;

import com.epam.capstone.models.Post;
import com.epam.capstone.models.Role;
import com.epam.capstone.models.Subscription;
import com.epam.capstone.models.User;
import com.epam.capstone.services.SecurityService;
import com.epam.capstone.services.SubscriptionService;
import com.epam.capstone.services.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class SecurityServiceImpl implements SecurityService {

    private final AuthenticationManager authenticationManager;
    private final UserDetailsService userDetailsService;

    private final SubscriptionService subscriptionService;

    private final UserService userService;

    public boolean isAuthenticated() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null || AnonymousAuthenticationToken.class.
                isAssignableFrom(authentication.getClass())) {
            return false;
        }
        return authentication.isAuthenticated();
    }

    @Override
    public void autoLogin(String username, String password) {
        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(userDetails, password, userDetails.getAuthorities());

        authenticationManager.authenticate(usernamePasswordAuthenticationToken);

        if (usernamePasswordAuthenticationToken.isAuthenticated()) {
            SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
            log.debug(String.format("Auto login %s successfully!", username));
        }
    }
    @Override
    public boolean isAccessible(Post post) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (post.getWeight() == 0) {
            return true;
        } else {
            if (authentication == null || AnonymousAuthenticationToken.class.
                    isAssignableFrom(authentication.getClass())) {
                return false;
            }
            User user = userService.findByUsername(authentication.getName());
            return checkAllRoles(user, post);
        }
    }

    @Override
    @Transactional
    public void makeModerator() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        List<GrantedAuthority> updatedAuthorities = new ArrayList<>(authentication.getAuthorities());
        User user = userService.addModeratorRole();
        updatedAuthorities.add(new SimpleGrantedAuthority("ROLE_MODERATOR"));
        UsernamePasswordAuthenticationToken newAuth = new UsernamePasswordAuthenticationToken(authentication.getPrincipal(),
                authentication.getCredentials(), updatedAuthorities);

        SecurityContextHolder.getContext().setAuthentication(newAuth);
        log.debug(String.format("Re - login %s successfully!", user.getUsername()));
    }


    private boolean checkAllRoles(User user, Post post){
        for (Role role : user.getRoles()) {
            if ("ROLE_ADMIN".equalsIgnoreCase(role.getName())) {
                return true;
            }
            if ("ROLE_MODERATOR".equalsIgnoreCase(role.getName()) &&
                    user.getUsername().equalsIgnoreCase(post.getAuthor().getUsername())) {
                return true;
            }
        }

        List<Subscription> subs = subscriptionService.findByAuthorAndReader(post.getAuthor().getUsername(),
                user.getUsername());

        for (Subscription sub: subs) {
            if (sub.getRole().getWeight() >= post.getWeight()) {
                return true;
            }
        }

        return false;
    }
}
