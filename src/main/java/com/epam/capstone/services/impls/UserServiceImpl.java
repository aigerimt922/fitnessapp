package com.epam.capstone.services.impls;

import com.epam.capstone.dto.UserRegisterDTO;
import com.epam.capstone.dto.UserWriterDTO;
import com.epam.capstone.mappers.UserMapper;
import com.epam.capstone.models.Post;
import com.epam.capstone.models.Role;
import com.epam.capstone.models.User;
import com.epam.capstone.repositories.UserRepository;
import com.epam.capstone.services.RoleService;
import com.epam.capstone.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    private final RoleService roleService;

    @Override
    public void save(UserRegisterDTO user) {
        User newUser = UserMapper.MAPPER.userRegisterDtoToUser(user);
        newUser.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        Set<Role> roles = new HashSet<>();
        roles.add(roleService.getDefaultRole());
        newUser.setRoles(roles);
        userRepository.save(newUser);
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    @Transactional
    public List<UserWriterDTO> findCreators() {
        List<User> users = userRepository.findAll();
        List<UserWriterDTO> writers = new ArrayList<>();
        for (User user : users) {
            if (user.getPosts() != null && !user.getPosts().isEmpty()) {
                writers.add(UserMapper.MAPPER.userToUserWriterDto(user));
            }
        }
        return writers;
    }

    @Override
    public User addModeratorRole() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = findByUsername(authentication.getName());
        Set<Role> roles = user.getRoles();
        Role role = roleService.getModeratorRole();
        if (userRepository.findByUsernameAndRolesContains(user.getUsername(), role) == null) {
            roles.add(role);
            user.setRoles(roles);
        }
        return userRepository.save(user);
    }

    public boolean checkIfMine(Post post) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return post.getAuthor().getUsername().equals(authentication.getName());
    }
}
