package com.epam.capstone.services.impls;

import com.epam.capstone.models.Tag;
import com.epam.capstone.repositories.TagRepository;
import com.epam.capstone.services.TagService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.HashSet;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TagServiceImpl implements TagService {

    private final TagRepository tagRepository;

    @Override
    public List<Tag> allTags() {
        return tagRepository.findAll();
    }

    @Override
    public Tag saveTag(Tag tag) {
        return tagRepository.save(tag);
    }

    @Override
    public void deleteTag(Integer id) {
        tagRepository.deleteById(id);
    }


    @Override
    public Tag findTag(Integer id) {
        return tagRepository.findById(id).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public Tag findTagByName(String name) {
        return tagRepository.findByName(name);
    }
}
