package com.epam.capstone.services.impls;

import com.epam.capstone.dto.SubscriptionDTO;
import com.epam.capstone.models.Role;
import com.epam.capstone.models.Subscription;
import com.epam.capstone.repositories.SubscriptionRepository;
import com.epam.capstone.services.RoleService;
import com.epam.capstone.services.SubscriptionService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SubscriptionServiceImpl implements SubscriptionService {

    private final SubscriptionRepository subscriptionRepository;

    private final RoleService roleService;

    @Override
    public List<Subscription> findAllSubs() {
        return subscriptionRepository.findAll();
    }

    @Override
    public void deleteSub(int id) {
        subscriptionRepository.deleteById(id);
    }

    @Override
    public void saveSub(SubscriptionDTO subscriptionDTO) {
        Role role = roleService.findRole(subscriptionDTO.getRole());
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        List<Subscription> subs = subscriptionRepository.findAllByAuthorAndReader(authentication.getName(),
                subscriptionDTO.getReader());

        for (Subscription sub : subs) {
            if (sub.getRole().getWeight() >= role.getWeight()) {
                throw new IllegalArgumentException("this reader already have level greater or equal than given");
            }
        }

        Subscription subscription = Subscription.builder()
                .author(authentication.getName())
                .reader(subscriptionDTO.getReader())
                .role(role)
                .build();

        subscriptionRepository.save(subscription);
    }

    @Override
    public List<Subscription> findByAuthorAndReader(String author, String reader) {
        return subscriptionRepository.findAllByAuthorAndReader(author, reader);
    }
}
