package com.epam.capstone.services.impls;

import com.epam.capstone.dto.PostDTO;
import com.epam.capstone.dto.PostPreviewDTO;
import com.epam.capstone.mappers.PostMapper;
import com.epam.capstone.models.Post;
import com.epam.capstone.models.Tag;
import com.epam.capstone.models.User;
import com.epam.capstone.repositories.PostRepository;
import com.epam.capstone.services.PostService;
import com.epam.capstone.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PostServiceImpl implements PostService {

    private final PostRepository postRepository;

    private final UserService userService;

    @Override
    public List<PostPreviewDTO> findAllPostsByAuthor(User user) {
        List<Post> posts = postRepository.findAllByAuthor(user);
        return populatePostPreview(posts);
    }

    @Override
    public List<PostPreviewDTO> findAllWithTagAndAuthor(Tag tag, User user) {
        List<Post> posts = postRepository.findAllByTagsContainsAndAuthor(tag, user);
        return populatePostPreview(posts);
    }


    @Override
    public Post findPost(Integer id) {
        return postRepository.findById(id).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public Post savePost(PostDTO post) {
        User user = userService.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        post.setAuthor(user);
        post.setDateOfCreation(LocalDateTime.now());
        return postRepository.save(PostMapper.MAPPER.postDtoToPost(post));
    }

    @Override
    public void deletePost(Integer id) {
        postRepository.deleteById(id);
    }

    private List<PostPreviewDTO> populatePostPreview(List<Post> posts) {
        List<PostPreviewDTO> previews = new ArrayList<>();
        for (Post post : posts) {
            previews.add(PostMapper.MAPPER.postToPostPreviewDto(post));
        }
        return previews;
    }
}
