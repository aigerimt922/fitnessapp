package com.epam.capstone.services;

import com.epam.capstone.dto.UserRegisterDTO;
import com.epam.capstone.dto.UserWriterDTO;
import com.epam.capstone.models.Post;
import com.epam.capstone.models.Role;
import com.epam.capstone.models.User;

import java.util.List;

public interface UserService {
    void save(UserRegisterDTO user);

    User findByUsername(String username);

    User findByEmail(String email);

    List<UserWriterDTO> findCreators();

    User addModeratorRole();

    boolean checkIfMine(Post post);
}
