package com.epam.capstone.services;

import com.epam.capstone.models.Tag;

import java.util.List;

public interface TagService {

    List<Tag> allTags();

    Tag saveTag(Tag tag);

    void deleteTag(Integer id);

    Tag findTag(Integer id);

    Tag findTagByName(String name);
}
