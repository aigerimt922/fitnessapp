package com.epam.capstone.services;

import com.epam.capstone.dto.UserRegisterDTO;
import com.epam.capstone.models.Post;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

public interface ResultService {

    void populateResultByStatus(Object status, Model model);

    String getPageAndAccessPostResult(Post post, Model model);

    /**
     *  if accessible then post saved into model
     *  if forbidden error message saved into model
     */
    boolean isPostForbidden(String postId, Model model);

    boolean isRegistrationFailed(UserRegisterDTO userForm, BindingResult bindingResult);

}
