package com.epam.capstone.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserWriterDTO {

    private String username;

    @Size(max = 32, message = "{bio.min.size}")
    private String bio;
}
