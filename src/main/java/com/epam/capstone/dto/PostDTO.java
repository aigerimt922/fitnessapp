package com.epam.capstone.dto;

import com.epam.capstone.models.Tag;
import com.epam.capstone.models.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PostDTO {

    protected Integer id;

    private String cover;

    private Integer weight;

    private String title;

    private String brief;

    private String description;

    private User author;

    private LocalDateTime dateOfCreation;

    private List<Tag> tags;
}
