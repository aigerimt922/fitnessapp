package com.epam.capstone.dto;

import com.epam.capstone.models.Tag;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PostPreviewDTO {

    private Integer id;

    private String cover;

    private Integer weight;

    private LocalDateTime dateOfCreation;

    private String title;

    private String brief;

    private List<Tag> tags;
}
