package com.epam.capstone.dto;

import com.epam.capstone.validation.PasswordMatch;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
@PasswordMatch
@Builder
public class UserRegisterDTO {

    @Size(min = 6, message = "{username.min.size}")
    private String username;

    @Email(message = "{email.validation}")
    private String email;

    @Pattern(regexp = "((?=.*[A-Z]).{6,32})", message = "{password.requirements}")
    private String password;

    private String passwordConfirm;
}
