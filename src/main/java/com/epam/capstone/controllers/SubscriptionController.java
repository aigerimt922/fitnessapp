package com.epam.capstone.controllers;

import com.epam.capstone.aspects.Loggable;
import com.epam.capstone.dto.SubscriptionDTO;
import com.epam.capstone.services.RoleService;
import com.epam.capstone.services.SubscriptionService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequiredArgsConstructor
@RequestMapping("/subscription")
public class SubscriptionController {

    private final SubscriptionService subscriptionService;

    private final RoleService roleService;

    private static final String ERROR = "error";

    @GetMapping("/all")
    @Loggable
    public String getSubs(Model model, String error) {
        if (error != null) {
            model.addAttribute(ERROR, error);
        }
        model.addAttribute("roles", roleService.findAllRoles());
        model.addAttribute("subscriptions", subscriptionService.findAllSubs());
        return "subscriptions";
    }

    @PostMapping("/save")
    @Loggable
    public String saveRole(SubscriptionDTO subDTO, RedirectAttributes redirectAttributes) {
        try {
            subscriptionService.saveSub(subDTO);
        } catch (IllegalArgumentException e) {
            redirectAttributes.addAttribute(ERROR, e.getMessage());
        }
        return "redirect:/subscription/all";
    }

    @DeleteMapping("/delete")
    @Loggable
    public String deleteRole(String subId) {
        subscriptionService.deleteSub(Integer.parseInt(subId));
        return "redirect:/subscription/all";
    }
}
