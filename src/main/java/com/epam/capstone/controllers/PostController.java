package com.epam.capstone.controllers;

import com.epam.capstone.aspects.Loggable;
import com.epam.capstone.dto.PostDTO;
import com.epam.capstone.models.Post;
import com.epam.capstone.models.Tag;
import com.epam.capstone.models.User;
import com.epam.capstone.services.*;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * @author Aigerim Tumbayeva
 */

@Controller
@RequiredArgsConstructor
@RequestMapping("/article")
public class PostController {

    private final PostService postService;
    private final UserService userService;
    private final ResultService resultService;
    private final TagService tagService;

    private final RoleService roleService;

    private static final String POSTS = "posts";


    @GetMapping("/all")
    @Loggable
    public String posts(Model model, String username) {
        User user = userService.findByUsername(username);
        model.addAttribute(POSTS, postService.findAllPostsByAuthor(user));
        fillPostsModel(model, user);
        return POSTS;
    }

    @GetMapping("/tag")
    @Loggable
    public String getByTag(Model model, String tagId, String username) {
        Tag tag = tagService.findTag(Integer.valueOf(tagId));
        User user = userService.findByUsername(username);
        model.addAttribute(POSTS, postService.findAllWithTagAndAuthor(tag, user));
        fillPostsModel(model, user);

        return POSTS;
    }

    @GetMapping()
    @Loggable
    public String getPost(Model model, String postId) {
        Post post = postService.findPost(Integer.valueOf(postId));
        return resultService.getPageAndAccessPostResult(post, model);
    }

    @GetMapping("/user")
    @Loggable
    public String getMyPosts(Model model) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = userService.findByUsername(username);
        model.addAttribute(POSTS, postService.findAllPostsByAuthor(user));
        return "myPosts";
    }

    @GetMapping("/editPostPage")
    @Loggable
    public String getPageForPostEdit(Model model, String postId) {
        if (resultService.isPostForbidden(postId, model)) {
            return "error";
        }
        model.addAttribute("roles", roleService.findAllRoles());
        model.addAttribute("tags", tagService.allTags());
        return "editPost";
    }

    @PostMapping("/save")
    @Loggable
    public String savePost(@ModelAttribute("post") PostDTO post) {
        postService.savePost(post);
        return "redirect:/article/user";
    }

    @DeleteMapping("/delete")
    @Loggable
    public String deletePost(String postId) {
        if (postId != null) {
            postService.deletePost(Integer.valueOf(postId));
        }
        return "redirect:/article/user";
    }

    private void fillPostsModel(Model model, User user) {
        model.addAttribute("username", user.getUsername());
        model.addAttribute("email", user.getEmail());
        model.addAttribute("roles", roleService.findAllRoles());
    }
}
