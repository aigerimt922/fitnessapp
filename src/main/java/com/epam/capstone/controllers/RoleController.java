package com.epam.capstone.controllers;

import com.epam.capstone.aspects.Loggable;
import com.epam.capstone.dto.RoleDTO;
import com.epam.capstone.models.Role;
import com.epam.capstone.services.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequiredArgsConstructor
@RequestMapping("/role")
public class RoleController {

    private final RoleService roleService;

    private static final String ERROR = "error";

    @GetMapping("/all")
    @Loggable
    public String getRoles(Model model, String error) {
        if (error != null) {
            model.addAttribute(ERROR, error);
        }
        model.addAttribute("roles", roleService.findAllRoles());
        return "roles";
    }

    @GetMapping("/editRolePage")
    @Loggable
    public String getPageForRoleEdit(Model model, String roleId, String error) {
        if (error != null) {
            model.addAttribute(ERROR, error);
        }
        if (roleId != null && !roleId.isEmpty()) {
            model.addAttribute("role", roleService.findRole(Integer.parseInt(roleId)));
        } else {
            model.addAttribute("role", new Role());
        }
        return "editRole";
    }

    @PostMapping("/save")
    @Loggable
    public String saveRole(@ModelAttribute("role") RoleDTO roleDTO, RedirectAttributes redirectAttributes) {
        try {
            roleService.saveRole(roleDTO);
        } catch (IllegalArgumentException e) {
            redirectAttributes.addAttribute(ERROR, e.getMessage());
            return "redirect:/role/editRolePage";
        }
        return "redirect:/role/all";
    }

    @DeleteMapping("/delete")
    @Loggable
    public String deleteRole(String roleId, RedirectAttributes redirectAttributes) {
        try {
            roleService.deleteRole(Integer.parseInt(roleId));
        } catch (IllegalArgumentException e) {
            redirectAttributes.addAttribute(ERROR, e.getMessage());
        }
        return "redirect:/role/all";
    }
}
