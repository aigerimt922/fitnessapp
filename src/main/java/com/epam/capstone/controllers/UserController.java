package com.epam.capstone.controllers;

import com.epam.capstone.aspects.Loggable;
import com.epam.capstone.dto.UserRegisterDTO;
import com.epam.capstone.services.ResultService;
import com.epam.capstone.services.SecurityService;
import com.epam.capstone.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

/**
 * @author Aigerim Tumbayeva
 */

@Controller
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;
    private final SecurityService securityService;

    private final ResultService resultService;

    private static final String USER_FORM = "userForm";
    private static final String REGISTRATION = "registration";
    private static final String REDIRECT_HOME = "redirect:/";


    @GetMapping("/registration")
    @Loggable
    public String registration(Model model) {
        if (securityService.isAuthenticated()) {
            return REDIRECT_HOME;
        }
        model.addAttribute(USER_FORM, new UserRegisterDTO());
        return REGISTRATION;
    }

    @PostMapping("/registration")
    @Loggable
    public String registration(@Valid @ModelAttribute(USER_FORM) UserRegisterDTO userForm,
                               BindingResult bindingResult) {

        if (resultService.isRegistrationFailed(userForm, bindingResult)) {
            return REGISTRATION;
        }
        userService.save(userForm);
        securityService.autoLogin(userForm.getUsername(), userForm.getPasswordConfirm());
        return REDIRECT_HOME;
    }

    @GetMapping("/login")
    @Loggable
    public String login(Model model, String error) {
        if (securityService.isAuthenticated()) {
            return REDIRECT_HOME;
        }
        if (error != null)
            model.addAttribute("error", "Username or password is incorrect");
        return "login";
    }

    @PostMapping("/login")
    @Loggable
    public String login(@RequestParam("username") String username,
                        @RequestParam("password") String password) {
        try {
            securityService.autoLogin(username, password);
        } catch (AuthenticationException e) {
            return "redirect:/login?error";
        }
        return REDIRECT_HOME;
    }

    @GetMapping("/makeModerator")
    @Loggable
    public String makeModerator() {
        securityService.makeModerator();
        return REDIRECT_HOME;
    }

    @GetMapping({"/", "/home"})
    @Loggable
    public String welcome(Model model) {
        model.addAttribute("users", userService.findCreators());
        return "index";
    }
}
