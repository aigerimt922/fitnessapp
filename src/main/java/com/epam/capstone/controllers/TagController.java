package com.epam.capstone.controllers;

import com.epam.capstone.aspects.Loggable;
import com.epam.capstone.models.Tag;
import com.epam.capstone.services.TagService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Aigerim Tumbayeva
 */

@Controller
@RequiredArgsConstructor
@RequestMapping("/tag")
public class TagController {

    private final TagService tagService;

    @Value("${tag.exist}")
    private String tagExist;

    @PostMapping("/save")
    @Loggable
    public String saveTag(String name) {
        if (tagService.findTagByName(name) == null) {
            Tag tag = new Tag();
            tag.setName(name);
            tagService.saveTag(tag);
        } else {
            return "redirect:/tag/all?error";
        }
        return "redirect:/tag/all";
    }

    @DeleteMapping("/delete")
    @Loggable
    public String deleteTag(String tagId) {
        if (tagId != null) {
            tagService.deleteTag(Integer.valueOf(tagId));
        }
        return "redirect:/tag/all/admin";
    }

    @GetMapping("/all")
    @Loggable
    public String tags(Model model, String error) {
        if (error != null) {
            model.addAttribute("error", tagExist);
        }
        model.addAttribute("tags", tagService.allTags());
        return "tags";
    }

    @GetMapping("/all/admin")
    @Loggable
    public String tagsForAdmin(Model model) {
        model.addAttribute("tags", tagService.allTags());
        return "deleteTags";
    }
}
