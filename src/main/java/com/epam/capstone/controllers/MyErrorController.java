package com.epam.capstone.controllers;


import com.epam.capstone.services.ResultService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;


@Controller
@RequiredArgsConstructor
public class MyErrorController implements ErrorController {

    private final ResultService resultService;
    private static final String PAGE_ERROR = "error";

    @RequestMapping("/error")
    public String handleError(HttpServletRequest request, Model model) {
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
        resultService.populateResultByStatus(status, model);
        return PAGE_ERROR;
    }
}
