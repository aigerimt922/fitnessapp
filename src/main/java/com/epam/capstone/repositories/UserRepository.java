package com.epam.capstone.repositories;

import com.epam.capstone.models.Role;
import com.epam.capstone.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface UserRepository extends JpaRepository<User, Integer> {

    User findByUsername(String username);

    User findByEmail(String email);

    User findByUsernameAndRolesContains(String username, Role role);
}
