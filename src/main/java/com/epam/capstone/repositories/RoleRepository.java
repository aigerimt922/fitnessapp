package com.epam.capstone.repositories;


import com.epam.capstone.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer>{
    Role findByName(String name);

    @Query("select r from Role r where r.weight>=0")
    List<Role> findAllExcept();

    Role findByWeight(int weight);
}
