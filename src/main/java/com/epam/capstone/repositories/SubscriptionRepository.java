package com.epam.capstone.repositories;

import com.epam.capstone.models.Subscription;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SubscriptionRepository extends JpaRepository<Subscription, Integer> {

    List<Subscription> findAllByAuthorAndReader(String author, String reader);
}
