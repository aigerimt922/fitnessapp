package com.epam.capstone.repositories;

import com.epam.capstone.models.Post;
import com.epam.capstone.models.Tag;
import com.epam.capstone.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface PostRepository extends JpaRepository<Post, Integer> {

    List<Post> findAllByAuthor(User user);

    List<Post> findAllByTagsContainsAndAuthor(Tag tag, User user);
}
