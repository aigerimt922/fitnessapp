package com.epam.capstone.validation;

import com.epam.capstone.dto.UserRegisterDTO;
import org.springframework.beans.factory.annotation.Value;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * A class for password confirmation
 * @see PasswordMatch
 *
 * @author Aigerim Tumbayeva
 */
public class PasswordMatchValidator implements ConstraintValidator<PasswordMatch, UserRegisterDTO> {

    @Value("${password.mismatch}")
    private String mismatch;

    @Override
    public void initialize(PasswordMatch p) {
        // do not need implementation
    }

    public boolean isValid(UserRegisterDTO user, ConstraintValidatorContext c) {
        c.disableDefaultConstraintViolation();
        c.buildConstraintViolationWithTemplate(mismatch).addPropertyNode("passwordConfirm").addConstraintViolation();

        String plainPassword = user.getPassword();
        String repeatPassword = user.getPasswordConfirm();

        return plainPassword != null && plainPassword.equals(repeatPassword);
    }

}
