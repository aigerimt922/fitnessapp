package com.epam.capstone.mappers;

import com.epam.capstone.dto.PostDTO;
import com.epam.capstone.dto.PostPreviewDTO;
import com.epam.capstone.models.Post;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface PostMapper {
    PostMapper MAPPER = Mappers.getMapper(PostMapper.class);
    Post postDtoToPost(PostDTO post);
    PostPreviewDTO postToPostPreviewDto(Post post);
}
