package com.epam.capstone.mappers;

import com.epam.capstone.dto.UserRegisterDTO;
import com.epam.capstone.dto.UserWriterDTO;
import com.epam.capstone.models.User;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface UserMapper {
    UserMapper MAPPER = Mappers.getMapper(UserMapper.class);
    User userRegisterDtoToUser(UserRegisterDTO userRegisterDTO);
    UserWriterDTO userToUserWriterDto (User user);
}
