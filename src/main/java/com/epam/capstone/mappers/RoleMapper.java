package com.epam.capstone.mappers;

import com.epam.capstone.dto.RoleDTO;
import com.epam.capstone.models.Role;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface RoleMapper {

    RoleMapper MAPPER = Mappers.getMapper(RoleMapper.class);

    Role roleDtoToRole(RoleDTO roleDTO);
}
