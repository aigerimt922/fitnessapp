package com.epam.capstone.aspects;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.CodeSignature;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Aspect
@Component
@Slf4j
public class LoggingAspect {

    private final ObjectMapper mapper;

    public LoggingAspect(ObjectMapper mapper) {
        this.mapper = mapper;
        mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
    }

    @Pointcut("@annotation(com.epam.capstone.aspects.Loggable)")
    public void pointcut() {
        // this method do not need implementation
    }

    @Before("pointcut()")
    public void logMethod(JoinPoint joinPoint) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();

        Map<String, Object> parameters = getParameters(joinPoint);
        try {
            log.info("==> method(s): {}, arguments: {} ",
                    signature.getMethod().getName(), Optional.of(mapper.writeValueAsString(parameters)));
        } catch (JsonProcessingException e) {
            log.error("Error while converting", e);
        }
    }

    @AfterReturning(pointcut = "pointcut()", returning = "view")
    public void logMethodAfter(JoinPoint joinPoint, String view) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();

        log.info("<== method(s): {}, retuning: {}",
                signature.getMethod().getName(), Optional.of(view));
    }

    private Map<String, Object> getParameters(JoinPoint joinPoint) {
        CodeSignature signature = (CodeSignature) joinPoint.getSignature();

        HashMap<String, Object> map = new HashMap<>();

        String[] parameterNames = signature.getParameterNames();

        for (int i = 0; i < parameterNames.length; i++) {
            if (!parameterNames[i].equalsIgnoreCase("bindingResult")) {
                map.put(parameterNames[i], joinPoint.getArgs()[i]);
            }
        }

        return map;
    }

}
