package com.epam.capstone.models;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.util.Set;


@NoArgsConstructor
@Getter
@Setter
@ToString@Builder

@AllArgsConstructor
@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(unique=true)
    private String username;

    private String password;

    private String bio;

    @OneToMany(mappedBy = "author", cascade = CascadeType.ALL)
    @ToString.Exclude
    @JsonManagedReference
    private Set<Post> posts;

    @Column(unique=true)
    private String email;
    @ManyToMany
    @ToString.Exclude
    @JsonManagedReference
    private Set<Role> roles;
}
