package com.epam.capstone.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@NoArgsConstructor
@Getter
@Setter
@ToString
@AllArgsConstructor
@Entity
@Builder
@Table(name = "posts")
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Integer id;

    private String cover;

    private Integer weight;

    private String title;

    private String brief;

    @Lob
    private String description;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    @JsonBackReference
    private User author;

    @Column(name = "creation_date")
    private LocalDateTime dateOfCreation;

    @ManyToMany()
    @ToString.Exclude
    @JsonManagedReference
    private List<Tag> tags;
}
