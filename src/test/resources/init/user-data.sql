INSERT INTO users (id, email,username, password)
VALUES   (1,
          'aigerimt922@gmail.com',
          'ATumbayeva',
          '$2a$10$RM2R9u6uvrYDmD7X6B/oOOvd6O2vLDfSbZhfvOPjbQQ21CXHmHgU6');

INSERT INTO roles (id, description, name, title, weight)
VALUES (1, '', 'ROLE_ADMIN','Admin',-2);

INSERT INTO users_roles
(users_id, roles_id) VALUES (1, 1);