package com.epam.capstone.services.impls;

import com.epam.capstone.models.Role;
import com.epam.capstone.models.User;
import com.epam.capstone.repositories.UserRepository;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;
import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@ExtendWith(MockitoExtension.class)
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class UserDetailsServiceImplTest {

    @InjectMocks
    private UserDetailsServiceImpl service;

    @Mock
    private UserRepository repository;

    @Test
    void should_load_user_details_by_username() {
        // Arrange
        final var user = User.builder().username("Lilo").password("password")
                .roles(Collections.singleton(Role.builder().name("role").build())).build();
        final var expected = new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
                Collections.singleton(new SimpleGrantedAuthority("role")));
        when(repository.findByUsername(anyString())).thenReturn(user);

        // Act
        final var actual = service.loadUserByUsername("Lilo");

        // Assert
        assertThat(actual).usingRecursiveComparison().isEqualTo(expected);
        verify(repository, times(1)).findByUsername(anyString());
        verifyNoMoreInteractions(repository);
    }
}