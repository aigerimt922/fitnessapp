package com.epam.capstone.services.impls;

import com.epam.capstone.dto.PostDTO;
import com.epam.capstone.models.Post;
import com.epam.capstone.models.Tag;
import com.epam.capstone.models.User;
import com.epam.capstone.repositories.PostRepository;
import com.epam.capstone.services.UserService;
import com.epam.capstone.utils.DataUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class PostServiceImplTest {

    @InjectMocks
    private PostServiceImpl service;
    @Mock
    private PostRepository repository;
    @Mock
    private UserService userService;
    @Mock
    private SecurityContext securityContext;

    private static final String testUsername = "Milly";

    @Test
    void should_find_all_posts_by_author() {
        // Arrange
        when(repository.findAllByAuthor(any(User.class))).thenReturn(List.of(new Post(), new Post()));

        // Act & Assert
        assertThat(service.findAllPostsByAuthor(new User())).hasSize(2);
        verify(repository, times(1)).findAllByAuthor(any(User.class));
        verifyNoMoreInteractions(repository);
    }

    @Test
    void should_find_all_posts_by_author_and_tag() {
        // Arrange
        when(repository.findAllByTagsContainsAndAuthor(any(Tag.class), any(User.class))).thenReturn(List.of(new Post(), new Post()));

        // Act & Assert
        assertThat(service.findAllWithTagAndAuthor(new Tag(), new User())).hasSize(2);
        verify(repository, times(1)).findAllByTagsContainsAndAuthor(any(Tag.class), any(User.class));
        verifyNoMoreInteractions(repository);
    }

    @Test
    void should_find_and_return_one_post() {
        // Arrange
        final var expectedPost = Post.builder().title("My post").weight(1).build();
        when(repository.findById(anyInt())).thenReturn(Optional.of(expectedPost));

        // Act
        final var actual = service.findPost(DataUtil.getRandomInteger());

        // Assert
        assertThat(actual).usingRecursiveComparison().isEqualTo(expectedPost);
        verify(repository, times(1)).findById(anyInt());
        verifyNoMoreInteractions(repository);
    }

    @Test
    void should_not_found_a_post_that_doesnt_exists() {
        // Arrange
        when(repository.findById(anyInt())).thenReturn(Optional.empty());

        // Act & Assert
        Assertions.assertThrows(EntityNotFoundException.class, () -> service.findPost(DataUtil.getRandomInteger()));
        verify(repository, times(1)).findById(anyInt());
        verifyNoMoreInteractions(repository);
    }

    @Test
    void should_save_one_post() {
        // Arrange
        final var expectedPost = Post.builder().title("My post").weight(1).build();
        when(repository.save(any(Post.class))).thenReturn(expectedPost);
        when(securityContext.getAuthentication()).thenReturn(new UsernamePasswordAuthenticationToken(testUsername, ""));
        SecurityContextHolder.setContext(securityContext);
        when(userService.findByUsername(testUsername)).thenReturn(new User());

        // Act
        final var actual = service.savePost(new PostDTO());

        // Assert
        assertThat(actual).usingRecursiveComparison().isEqualTo(expectedPost);
        verify(repository, times(1)).save(any(Post.class));
        verify(userService, times(1)).findByUsername(testUsername);
        verifyNoMoreInteractions(repository);
        verifyNoMoreInteractions(userService);
    }

    @Test
    void should_delete_one_post() {
        // Arrange
        doNothing().when(repository).deleteById(anyInt());

        // Act & Assert
        service.deletePost(DataUtil.getRandomInteger());
        verify(repository, times(1)).deleteById(anyInt());
        verifyNoMoreInteractions(repository);
    }
}