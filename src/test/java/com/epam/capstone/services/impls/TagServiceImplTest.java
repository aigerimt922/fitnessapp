package com.epam.capstone.services.impls;

import com.epam.capstone.models.Tag;
import com.epam.capstone.repositories.TagRepository;
import com.epam.capstone.utils.DataUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class TagServiceImplTest {

    @InjectMocks
    private TagServiceImpl service;

    @Mock
    private TagRepository repository;

    @Test
    void should_find_and_return_all_tags() {
        // Arrange
        when(repository.findAll()).thenReturn(List.of(new Tag(), new Tag()));

        // Act & Assert
        assertThat(service.allTags()).hasSize(2);
        verify(repository, times(1)).findAll();
        verifyNoMoreInteractions(repository);
    }

    @Test
    void should_save_one_tag() {
        // Arrange
        final var tagToSave = Tag.builder().name("tabata").build();
        when(repository.save(any(Tag.class))).thenReturn(tagToSave);

        // Act
        final var actual = service.saveTag(new Tag());

        // Assert
        assertThat(actual).usingRecursiveComparison().isEqualTo(tagToSave);
        verify(repository, times(1)).save(any(Tag.class));
        verifyNoMoreInteractions(repository);
    }

    @Test
    void should_delete_one_tag() {
        // Arrange
        doNothing().when(repository).deleteById(anyInt());

        // Act & Assert
        service.deleteTag(DataUtil.getRandomInteger());
        verify(repository, times(1)).deleteById(anyInt());
        verifyNoMoreInteractions(repository);
    }

    @Test
    void should_find_and_return_one_tag() {
        // Arrange
        final var expected = Tag.builder().name("tabata").build();
        when(repository.findById(anyInt())).thenReturn(Optional.of(expected));

        // Act
        final var actual = service.findTag(DataUtil.getRandomInteger());

        // Assert
        assertThat(actual).usingRecursiveComparison().isEqualTo(expected);
        verify(repository, times(1)).findById(anyInt());
        verifyNoMoreInteractions(repository);
    }

    @Test
    void should_find_and_return_one_tag_by_name() {
        // Arrange
        final var expected = Tag.builder().name("tabata").build();
        when(repository.findByName(anyString())).thenReturn(expected);

        // Act
        final var actual = service.findTagByName("tabata");

        // Assert
        assertThat(actual).usingRecursiveComparison().isEqualTo(expected);
        verify(repository, times(1)).findByName(anyString());
        verifyNoMoreInteractions(repository);
    }

    @Test
    void should_not_found_a_tag_that_doesnt_exists() {
        // Arrange
        when(repository.findById(anyInt())).thenReturn(Optional.empty());

        // Act & Assert
        Assertions.assertThrows(EntityNotFoundException.class, () -> service.findTag(DataUtil.getRandomInteger()));
        verify(repository, times(1)).findById(anyInt());
        verifyNoMoreInteractions(repository);
    }

}