package com.epam.capstone.services.impls;

import com.epam.capstone.dto.UserRegisterDTO;
import com.epam.capstone.models.Post;
import com.epam.capstone.models.Role;
import com.epam.capstone.models.User;
import com.epam.capstone.repositories.UserRepository;
import com.epam.capstone.services.RoleService;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class UserServiceImplTest {

    @InjectMocks
    private UserServiceImpl service;

    @Mock
    private UserRepository repository;

    @Mock
    private RoleService roleService;

    @Mock
    private SecurityContext securityContext;

    @Mock
    private BCryptPasswordEncoder encoder;


    @Test
    void should_save_user() {
        // Arrange
        final var expected = User.builder().username("Lilo").build();
        when(repository.save(any(User.class))).thenReturn(expected);
        lenient().when(encoder.encode(eq("pass"))).thenReturn("pass");

        // Act
        service.save(UserRegisterDTO.builder().email("aigerimt922@gmail.com").build());

        // Assert
        verify(repository, times(1)).save(any(User.class));
        verifyNoMoreInteractions(repository);
    }

    @Test
    void should_find_and_return_one_user_by_username() {
        // Arrange
        final var expected = User.builder().username("Lilo").build();
        when(repository.findByUsername(anyString())).thenReturn(expected);

        // Act
        final var actual = service.findByUsername("Lilo");

        // Assert
        assertThat(actual).usingRecursiveComparison().isEqualTo(expected);
        verify(repository, times(1)).findByUsername(anyString());
        verifyNoMoreInteractions(repository);
    }

    @Test
    void should_find_and_return_one_user_by_email() {
        // Arrange
        final var expected = User.builder().email("aike@gmail.com").build();
        when(repository.findByEmail(anyString())).thenReturn(expected);

        // Act
        final var actual = service.findByEmail("aike@gmail.com");

        // Assert
        assertThat(actual).usingRecursiveComparison().isEqualTo(expected);
        verify(repository, times(1)).findByEmail(anyString());
        verifyNoMoreInteractions(repository);
    }

    @Test
    void should_find_and_return_all_users_with_posts() {
        // Arrange
        when(repository.findAll()).thenReturn(List.of(User.builder()
                .posts(Collections.singleton(new Post())).build(), new User()));

        // Act & Assert
        assertThat(service.findCreators()).hasSize(1);
        verify(repository, times(1)).findAll();
        verifyNoMoreInteractions(repository);
    }

    @Test
    void addModeratorRole() {
        // Arrange
        Role role = new Role();
        final var expected = User.builder().email("aike@gmail.com").roles(new HashSet<>()).build();
        when(securityContext.getAuthentication()).thenReturn(new UsernamePasswordAuthenticationToken("", ""));
        SecurityContextHolder.setContext(securityContext);
        when(roleService.getModeratorRole()).thenReturn(role);
        when(repository.save(any(User.class))).thenReturn(expected);
        when(service.findByUsername(anyString())).thenReturn(expected);
        lenient().when(repository.findByUsernameAndRolesContains(anyString(), eq(role))).thenReturn(null);

        // Act
        final var actual = service.addModeratorRole();

        // Assert
        expected.setRoles(Collections.singleton(role));
        assertThat(actual).usingRecursiveComparison().isEqualTo(expected);
        verify(roleService, times(1)).getModeratorRole();
        verifyNoMoreInteractions(roleService);
    }

    @Test
    void checkIfMine() {
        // Arrange
        when(securityContext.getAuthentication()).thenReturn(new UsernamePasswordAuthenticationToken("Loli", ""));
        SecurityContextHolder.setContext(securityContext);

        // Act
        final var actual = service.checkIfMine(Post.builder().author(User.builder().username("Loli").build()).build());

        // Assert
        assertThat(actual).isTrue();
        verify(securityContext, times(1)).getAuthentication();
        verifyNoMoreInteractions(securityContext);
    }
}