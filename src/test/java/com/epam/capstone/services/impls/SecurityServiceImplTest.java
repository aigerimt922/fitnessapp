package com.epam.capstone.services.impls;

import com.epam.capstone.models.Post;
import com.epam.capstone.models.Role;
import com.epam.capstone.models.Subscription;
import com.epam.capstone.models.User;
import com.epam.capstone.services.SubscriptionService;
import com.epam.capstone.services.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.Collections;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.params.provider.Arguments.arguments;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class SecurityServiceImplTest {
    @InjectMocks
    private SecurityServiceImpl service;
    @Mock
    private AuthenticationManager authenticationManager;
    @Mock
    private UserService userService;
    @Mock
    private UserDetailsService userDetailsService;
    @Mock
    private SecurityContext securityContext;

    @Mock
    private SubscriptionService subscriptionService;
    private Authentication authentication;
    private UserDetails userDetails;
    private static User user;

    @BeforeEach
    void init() {
        userDetails = new org.springframework.security.core.userdetails.User("name", "pass",
                Collections.singletonList(new SimpleGrantedAuthority("role")));
        authentication = new UsernamePasswordAuthenticationToken(userDetails, "password",
                userDetails.getAuthorities());
        user = User.builder().username("Me").build();

        // Common arrange
        lenient().when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
    }

    @Test
    void should_be_authenticated() {
        // Act
        final var actual = service.isAuthenticated();

        // Assert
        assertThat(actual).isTrue();
        verify(securityContext, times(1)).getAuthentication();
        verifyNoMoreInteractions(securityContext);
    }

    @Test
    void should_not_be_authenticated() {
        // Arrange
        when(securityContext.getAuthentication()).thenReturn(null);
        SecurityContextHolder.setContext(securityContext);

        // Act
        final var actual = service.isAuthenticated();

        // Assert
        assertThat(actual).isFalse();
        verify(securityContext, times(1)).getAuthentication();
        verifyNoMoreInteractions(securityContext);
    }

    @Test
    void should_authenticate() {
        // Arrange
        when(userDetailsService.loadUserByUsername(anyString())).thenReturn(userDetails);

        // Act
        service.autoLogin("name", "password");

        // Assert
        verify(userDetailsService, times(1)).loadUserByUsername(anyString());
        verify(authenticationManager, times(1)).authenticate(authentication);
        verifyNoMoreInteractions(authenticationManager);
    }


    @ParameterizedTest
    @MethodSource("goodWeights")
    void should_be_accessible_good_weights(Role role, Post post) {
        // Arrange
        user.setRoles(Collections.singleton(role));
        when(userService.findByUsername(anyString())).thenReturn(user);
        lenient().when(subscriptionService.findByAuthorAndReader(anyString(), anyString()))
                .thenReturn(Collections.singletonList(Subscription.builder().role(role).build()));

        // Act
        final var actual = service.isAccessible(post);

        // Assert
        assertThat(actual).isTrue();
        verify(userService, times(1)).findByUsername(anyString());
    }


    @ParameterizedTest
    @MethodSource("badWeights")
    void should_not_be_accessible_bad_weights(Role role, Post post) {
        // Arrange
        user.setRoles(Collections.singleton(role));
        when(userService.findByUsername(anyString())).thenReturn(user);
        when(subscriptionService.findByAuthorAndReader(anyString(), anyString()))
                .thenReturn(Collections.singletonList(Subscription.builder().role(role).build()));

        // Act
        final var actual = service.isAccessible(post);

        // Assert
        assertThat(actual).isFalse();
        verify(userService, times(1)).findByUsername(anyString());
    }

    @Test
    void should_not_be_accessible_auth_null() {
        // Arrange
        when(securityContext.getAuthentication()).thenReturn(null);
        SecurityContextHolder.setContext(securityContext);

        // Act
        final var actual = service.isAccessible(Post.builder().weight(1).build());

        // Assert
        assertThat(actual).isFalse();
        verify(securityContext, times(1)).getAuthentication();
        verifyNoMoreInteractions(securityContext);
    }

    @Test
    void should_be_accessible_to_everyone() {

        // Act
        final var actual = service.isAccessible(Post.builder().weight(0).build());

        // Assert
        assertThat(actual).isTrue();
        verify(securityContext, times(1)).getAuthentication();
        verifyNoMoreInteractions(securityContext);
    }

    private static Stream<Arguments> goodWeights() {
        return Stream.of(
                arguments(Role.builder().weight(1).build(), Post.builder().weight(1).author(user).build()),
                arguments(Role.builder().weight(2).build(), Post.builder().weight(1).author(user).build()),
                arguments(Role.builder().name("ROLE_MODERATOR").build(), Post.builder().weight(2).author(user).build()),
                arguments(Role.builder().name("ROLE_ADMIN").build(), Post.builder().weight(2).build())
        );
    }

    private static Stream<Arguments> badWeights() {
        return Stream.of(
                arguments(Role.builder().weight(1).build(), Post.builder().weight(2).author(user).build()),
                arguments(Role.builder().weight(-1).name("ROLE_MODERATOR").build(), Post.builder()
                        .author(User.builder().username("notMe").build()).weight(2).build())
        );
    }

    @Test
    void should_add_user_moderator_role() {
        // Arrange
        when(userService.addModeratorRole()).thenReturn(user);

        // Act
        service.makeModerator();

        // Assert
        verify(userService, times(1)).addModeratorRole();
        verify(securityContext, times(1)).getAuthentication();
        verifyNoMoreInteractions(userService);
    }
}