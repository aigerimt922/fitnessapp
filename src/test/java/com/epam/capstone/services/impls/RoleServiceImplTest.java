package com.epam.capstone.services.impls;

import com.epam.capstone.models.Role;
import com.epam.capstone.repositories.RoleRepository;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class RoleServiceImplTest {

    @InjectMocks
    private RoleServiceImpl service;

    @Mock
    private RoleRepository repository;

    @Test
    void should_find_and_return_user_role() {
        // Arrange
        final var expectedRole = Role.builder().name("ROLE_USER").build();
        when(repository.findByName(anyString())).thenReturn(expectedRole);

        // Act
        final var actual = service.getDefaultRole();

        // Assert
        assertThat(actual).usingRecursiveComparison().isEqualTo(expectedRole);
        verify(repository, times(1)).findByName(anyString());
        verifyNoMoreInteractions(repository);
    }

    @Test
    void should_not_found_then_create_and_return_user_role() {
        // Arrange
        final var expectedRole = Role.builder()
                .name("ROLE_USER")
                .weight(1)
                .description("Posts with this level can be opened if you registered")
                .title("Registered User")
                .build();
        when(repository.findByName(anyString())).thenReturn(null);
        when(repository.save(any(Role.class))).thenReturn(expectedRole);

        // Act
        final var actual = service.getDefaultRole();

        // Assert
        assertThat(actual).usingRecursiveComparison().isEqualTo(expectedRole);
        verify(repository, times(1)).findByName(anyString());
        verify(repository, times(1)).save(any(Role.class));
        verifyNoMoreInteractions(repository);
    }

    @Test
    void should_find_and_return_moderator_role() {
        // Arrange
        final var expectedRole = Role.builder().name("ROLE_MODERATOR").build();
        when(repository.findByName(anyString())).thenReturn(expectedRole);

        // Act
        final var actual = service.getModeratorRole();

        // Assert
        assertThat(actual).usingRecursiveComparison().isEqualTo(expectedRole);
        verify(repository, times(1)).findByName(anyString());
        verifyNoMoreInteractions(repository);
    }

    @Test
    void should_not_found_then_create_and_return_moderator_role() {
        // Arrange
        final var expectedRole = Role.builder()
                .name("ROLE_MODERATOR")
                .weight(-1)
                .build();
        when(repository.findByName(anyString())).thenReturn(null);
        when(repository.save(any(Role.class))).thenReturn(expectedRole);

        // Act
        final var actual = service.getModeratorRole();

        // Assert
        assertThat(actual).usingRecursiveComparison().isEqualTo(expectedRole);
        verify(repository, times(1)).findByName(anyString());
        verify(repository, times(1)).save(any(Role.class));
        verifyNoMoreInteractions(repository);
    }

    @Test
    void should_find_and_return_all_roles() {
        // Arrange
        when(repository.findAllExcept()).thenReturn(List.of(new Role(), new Role()));

        // Act & Assert
        assertThat(service.findAllRoles()).hasSize(2);
        verify(repository, times(1)).findAllExcept();
        verifyNoMoreInteractions(repository);
    }
}