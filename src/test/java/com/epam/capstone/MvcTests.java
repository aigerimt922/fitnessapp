package com.epam.capstone;

import com.epam.capstone.config.WebSecurityConfig;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.BEFORE_TEST_METHOD;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.log;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = {CapstoneApplication.class, WebSecurityConfig.class})
@AutoConfigureMockMvc
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
@ActiveProfiles("test")
@Transactional
class MvcTests {
    @Autowired
    private MockMvc mockMvc;

    private static RequestPostProcessor anybody() {
        return user("eddie").roles();
    }

    private static RequestPostProcessor registered() {
        return user("eddie").roles("USER");
    }

    private static RequestPostProcessor moderator() {
        return user("eddie").roles("MODERATOR");
    }

    private static RequestPostProcessor admin() {
        return user("eddie").roles("ADMIN");
    }

    @SqlGroup({
            @Sql(value = "classpath:empty/reset.sql", executionPhase = BEFORE_TEST_METHOD),
            @Sql(value = "classpath:init/user-data.sql", executionPhase = BEFORE_TEST_METHOD)
    })
    @Test
    void test_post_controller_endpoints() throws Exception {
        mockMvc.perform(get("/article/user").with(moderator()))
                .andDo(log())
                .andExpect(status().isOk())
                .andReturn();
        mockMvc.perform(get("/article/user").with(admin()))
                .andDo(log())
                .andExpect(status().isOk())
                .andReturn();
        mockMvc.perform(get("/article/user").with(registered()))
                .andDo(log())
                .andExpect(status().isForbidden())
                .andReturn();
        mockMvc.perform(get("/article/user").with(anybody()))
                .andDo(log())
                .andExpect(status().isForbidden())
                .andReturn();

        mockMvc.perform(get("/article/editPostPage").with(moderator()))
                .andDo(log())
                .andExpect(status().isOk())
                .andReturn();
        mockMvc.perform(get("/article/editPostPage").with(registered()))
                .andDo(log())
                .andExpect(status().isForbidden())
                .andReturn();

        mockMvc.perform(get("/article/all")
                        .param("username", "ATumbayeva")
                        .with(anybody()))
                .andDo(log())
                .andExpect(status().isOk())
                .andReturn();


        mockMvc.perform(post("/article/save")
                        .with(registered()))
                .andDo(log())
                .andExpect(status().isForbidden())
                .andReturn();
        mockMvc.perform(post("/article/save")
                        .with(moderator()))
                .andDo(log())
                .andExpect(status().is3xxRedirection())
                .andReturn();
        mockMvc.perform(post("/article/delete")
                        .with(registered()))
                .andDo(log())
                .andExpect(status().isForbidden())
                .andReturn();
        mockMvc.perform(delete("/article/delete")
                        .with(moderator()))
                .andDo(log())
                .andExpect(status().is3xxRedirection())
                .andReturn();
    }

    @Test
    void test_tag_controller_endpoints() throws Exception {
        mockMvc.perform(get("/tag/all")
                        .with(anybody()))
                .andDo(log())
                .andExpect(status().isForbidden())
                .andReturn();
        mockMvc.perform(get("/tag/all")
                        .with(moderator()))
                .andDo(log())
                .andExpect(status().isOk())
                .andReturn();


        mockMvc.perform(post("/tag/save")
                        .with(registered()))
                .andDo(log())
                .andExpect(status().isForbidden())
                .andReturn();
        mockMvc.perform(post("/tag/save")
                        .with(moderator()))
                .andDo(log())
                .andExpect(status().is3xxRedirection())
                .andReturn();
        mockMvc.perform(post("/tag/delete")
                        .with(registered()))
                .andDo(log())
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    void test_user_controller_endpoints() throws Exception {
        mockMvc.perform(get("/registration")
                        .with(anybody()))
                .andDo(log())
                .andExpect(status().is3xxRedirection())
                .andReturn();
        mockMvc.perform(get("/login")
                        .with(anybody()))
                .andDo(log())
                .andExpect(status().is3xxRedirection())
                .andReturn();
        mockMvc.perform(post("/registration")
                        .with(anybody()))
                .andDo(log())
                .andExpect(status().isOk())
                .andReturn();
        mockMvc.perform(post("/login")
                        .with(anybody()))
                .andDo(log())
                .andExpect(status().is3xxRedirection())
                .andReturn();

        mockMvc.perform(get("/error")
                        .with(anybody()))
                .andDo(log())
                .andExpect(status().isOk())
                .andReturn();
        mockMvc.perform(get("/home")
                        .with(anybody()))
                .andDo(log())
                .andExpect(status().isOk())
                .andReturn();
    }
}
