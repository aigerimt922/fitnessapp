package com.epam.capstone.utils;

import java.util.Random;

public class DataUtil {

    private DataUtil(){
    }

    public static int getRandomInteger() {
        return new Random().ints(1, 10).findFirst().orElse(1);
    }
}
